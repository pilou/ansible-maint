from imp import load_source
from os import path
import os
from setuptools import setup


ansiblemaint = load_source('version', path.join('ansiblemaint', 'version.py'))

setup(version=ansiblemaint.VERSION)
