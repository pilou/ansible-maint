This program lists namespace maintainers of https://github.com/ansible/ansible/ project.

This list can be used when requesting reviews from namespace maintainers when there is only one module maintainer or
when module maintainers aren't available.

============
Installation
============

Requirements
------------

The following packages are required:

- ``pip >= 6.0`` (version specifiers containing environment markers are used)
- ``setuptools >= 30.3.0`` (``setup.cfg`` used)

Command
-------

  $ pip install --process-dependency-links . 

================
Some definitions
================

**Maintainers** are allowed to approve pull requests.

**Namespace** of a module is composed of two directories below lib/ansible/modules (or one directory if there is only one).

  Examples:

  =============================================  ==================
                    module path                      namespace
  =============================================  ==================
  ``lib/ansible/modules/packaging/os/apt.py``    ``packaging/os``
  ``lib/ansible/modules/source_control/git.py``  ``source_control``
  =============================================  ==================

  `Source <https://github.com/ansible/ansibullbot/blob/ba1d1b66d18c822d7e8af71d14447054287180c5/ansibullbot/utils/moduletools.py#L586>`_.

**Module maintainers** are the authors of the module and people listed in `BOTMETA.yml <https://github.com/ansible/ansible/blob/devel/.github/BOTMETA.yml>`_.

**Module maintainers** of all modules in one namespace are called namespace maintainers.

Approvals/reviews
  Some pull requests require two approvals, others require only one. Once approved, a few pull requests are
  automatically merged, a majority need to be approved by the core team of the project. These rules are
  listed `here <https://github.com/ansible/ansibullbot/blob/master/ISSUE_HELP.md#for-pull-request-submitters>`_.

  Pull requests waiting for reviews are labeled with:

    - ``certified_review`` or ``core_review`` or ``community_review``,
    - and ``stale_ci`` or ``stale_review``.

Ansible® is a registered trademark of Ansible, Inc. in the United States and other countries.
