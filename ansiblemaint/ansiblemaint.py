#!/usr/bin/env python
# encoding: utf-8

# Copyright: 2017 Pierre-Louis Bonicoli <pierre-louis@libregerbil.fr>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import absolute_import, division, print_function
__metaclass__ = type

import argparse
from datetime import datetime, timedelta
import getpass
import logging
import os
from pathlib import PosixPath
import random
import re
import shutil
import subprocess
import sys
import time

from ansibullbot.parsers.botmetadata import BotMetadataParser
from ansibullbot.utils.moduletools import ModuleIndexer

import requests

API_URL = 'https://api.github.com'

LOGGER = logging.getLogger('maint')


def run_command(cmd, cwd=None):
    proc = subprocess.Popen(cmd.split(), cwd=cwd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = proc.communicate()

    if proc.returncode != 0:
        raise Exception('Command %r failed (%s):\n%s' % (cmd, proc.returncode, err))

    LOGGER.debug('Execute %r:\n%s', cmd, out.strip())


class AuthorIndexer(ModuleIndexer):
    def __init__(self, working_dir, reference=None):
        # pylint: disable=super-init-not-called
        self.emailmap = {}
        self.modules = {}
        self.maintainers = {}  # dict of maintainers, removed by #767
        self.reference = reference
        working_dir = PosixPath(working_dir)
        self.repo_dir = working_dir / 'ansible'

        self.setup(working_dir)

    def setup(self, working_dir):
        if not self.repo_dir.exists():
            if not working_dir.exists():
                working_dir.mkdir(parents=True)
            try:
                self.setup_repo(working_dir)
            except subprocess.CalledProcessError:
                shutil.rmtree(str(self.repo_dir), ignore_errors=True)
                raise

    def load_metadata(self, namespace=''):
        with (self.repo_dir / '.github/BOTMETA.yml').open() as data:
            self.botmeta = BotMetadataParser.parse_yaml(data.read())

        for filepath, value in self.botmeta['files'].items():
            if value and filepath.startswith('lib/ansible/modules/' + namespace):
                module = filepath[len('lib/ansible/modules/'):]  # prefix will be kept with #767
                self.maintainers[module] = set()
                if isinstance(value, dict):  # after #767, will be always a dict
                    for maintainer in value.get('maintainers', []):
                        self.maintainers[module].add(maintainer)

    @property
    def checkoutdir(self):
        return str(self.repo_dir)

    def setup_repo(self, working_dir):
        commands = [
            'git init ansible',
            'git remote add -t devel upstream https://github.com/ansible/ansible',
            'git symbolic-ref HEAD refs/heads/devel',  # set default branch
            'git config core.sparsecheckout true',
        ]

        run_command(commands[0], cwd=str(working_dir))
        for command in commands[1:]:
            run_command(command, cwd=str(self.repo_dir))

    def update_repo(self, namespace=''):
        if self.reference:
            # set reference repository
            with (self.repo_dir / '.git/objects/info/alternates').open('w') as alternates:
                alternates.write(os.path.join(str(PosixPath(self.reference).resolve()), u'.git/objects\n'))

        keep = [
            '.github/BOTMETA.yml',
            os.path.join('lib/ansible/modules/', namespace, '**')
        ]
        with (self.repo_dir / '.git/info/sparse-checkout').open('w') as sparse_checkout:
            sparse_checkout.writelines((u'%s\n' % k for k in keep))

        fetch = 'git fetch --depth=1 --no-tags upstream'
        run_command(fetch, cwd=str(self.repo_dir))
        checkout = 'git checkout --detach upstream/devel'
        run_command(checkout, cwd=str(self.repo_dir))

        read_tree = 'git read-tree -mu HEAD'
        run_command(read_tree, cwd=str(self.repo_dir))

    @staticmethod
    def loop(directory):
        for child in directory.iterdir():
            if child.is_file():
                yield child
            elif child.is_dir():
                for entry in AuthorIndexer.loop(child):
                    yield entry
            else:
                raise NotImplementedError()

    def list_authors(self, namespace=''):
        """populate self.modules with authors"""
        self.update_repo(namespace=namespace)
        self.load_metadata(namespace=namespace)

        for module in self.loop(self.repo_dir / 'lib/ansible/modules' / namespace):
            authors = self.get_module_authors(str(module))
            module = str(module).replace(str(self.repo_dir) + '/', '')

            LOGGER.debug('author of %s: %s', module, ' '.join(authors))

            self.modules[module] = {
                'filepath': module,
                'maintainers': sorted(authors)
            }
            self.modules[module].update(self.split_topics_from_path(module))

    def list_namespaces(self):
        modules = self.repo_dir / 'lib/ansible/modules'
        path_len = len(modules.parts)
        namespaces = set()
        for module in modules.glob('**/*.py'):
            if not module.name.endswith('__init__.py'):
                namespaces.add(os.path.join(*module.parent.parts[path_len:path_len+2]))
        return namespaces


def create_indexer(working_dir, reference=None, namespace=''):
    LOGGER.debug('Create indexer')
    exc_info = LOGGER.getEffectiveLevel() == logging.DEBUG
    try:
        author_indexer = AuthorIndexer(working_dir, reference=reference)
    except subprocess.CalledProcessError as exc:
        LOGGER.error('Command %r returned non-zero exit status (%s)', ' '.join(exc.cmd), exc.returncode,
                     exc_info=exc_info)
        sys.exit(1)

    try:
        # populate self.modules with authors
        author_indexer.list_authors()
    except subprocess.CalledProcessError as exc:
        LOGGER.error('Command %r returned non-zero exit status (%s)', ' '.join(exc.cmd), exc.returncode,
                     exc_info=exc_info)
        raise
    except OSError as exc:
        logging.error(exc, exc_info=exc_info)
        raise

    # add maintainers from BOTMETA.yml
    author_indexer.set_maintainers()

    return author_indexer


def get_next_page(headers):
    if 'link' not in headers:
        # when there is only one page, 'link' header is missing
        return

    rels = headers['link'].split(', ')
    for rel in rels:
        match = re.match(r'<(?P<url>[^>]+)>; rel="(?P<id>[^"]+)"', rel)
        if match.group('id') == 'next':
            return match.group('url')


def fetch_activity(headers, days, login=None):
    LOGGER.info('Fetch activity for %r', login)

    next_page = True
    url = '{}/users/{}/events?page=1'.format(API_URL, login)
    since = datetime.now() - timedelta(days=days)

    # first page contains the more recent events
    while next_page:
        response = requests.get(url, headers=headers)
        if response.status_code == 404:
            LOGGER.error('Invalid user %r', login)
            assert url == '{}/users/{}/events?page=1'.format(API_URL, login)  # should only occurrs on the first page
            return
        elif response.status_code != 200:

            LOGGER.error('HTTP status code: %r', response.status_code)
            if 'application/json' in response.headers.get('Content-Type', ''):
                LOGGER.error('HTTP Response: %r', response.json())
            else:
                LOGGER.error('HTTP Response: %r', response.text)
            LOGGER.error('HTTP headers: %r', response.headers)
            remaining = response.headers['X-RateLimit-Remaining']

            # Check if rate limit has been reached
            if response.status_code == 403 and int(remaining) == 0:
                reset_time = datetime.fromtimestamp(int(response.headers['X-RateLimit-Reset']))
                LOGGER.warn('Number of requests remaining %s (%s local time)', remaining, datetime.now())
                LOGGER.warn('Program will sleep until %s (UTC)', reset_time)
                duration = reset_time - datetime.utcnow()
                time.sleep(duration.total_seconds())
                LOGGER.info('resuming (%s local time)', datetime.now())
                continue
            else:
                raise Exception()
        for event in response.json():
            date = datetime.strptime(event['created_at'], '%Y-%m-%dT%H:%M:%SZ')
            if date < since:
                next_page = False
                # ignore 'old' events
                break

            if event['repo']['name'] != 'ansible/ansible':
                # ignore events unrelated to 'ansible/ansible'
                continue

            LOGGER.debug('user: %s event: %s created_at: %s', login, event['type'], event['created_at'])
            LOGGER.debug('Number of requests remaining: %s', response.headers['X-RateLimit-Remaining'])
            return {'login': login, 'created_at': event['created_at'], 'type': event['type']}
        else:
            url = get_next_page(response.headers)
            if not url:
                break

    return {'login': login}  # no recent event


def select_active_users(headers, users, days=14):
    active = set()

    if len(users) > 100:
        users = random.sample(users, 100)

    for user in users:
        if fetch_activity(headers, days, login=user):
            active.add(user)

    print("%s active users (%s tested)" % (len(active), len(users)))
    return active


def pick_contributors(contributors):
    if len(contributors) > 2:
        return random.sample(contributors, 2)

    return contributors


def create_parser():
    parser = argparse.ArgumentParser(description='List Ansible namespace maintainers')
    parser.add_argument('--filter-inactive', '-f', action='store', nargs='?', type=int, default=0, dest='timeout',
                        const=14, metavar='DAYS', help="maintainers without any GitHub activity on ansible/ansible"
                        "repository in the last DAYS aren't considered active (default: 14)")
    parser.add_argument('--namespace', '-n', required=True, action='store', dest='namespace')
    parser.add_argument('--reference', '-r', action='store', help='reference repository to use in order to reduce '
                        'network and local storage costs')
    parser.add_argument('--verbose', '-v', action='count', default=0, help='log level, repeat up to two times')
    parser.add_argument('--dir', '-d', action='store', dest='working_dir', help='set working directory')
    return parser


def get_default_working_dir():
    xdg_cache_home = os.getenv('XDG_CACHE_HOME')
    if xdg_cache_home:
        working_dir = os.path.join(xdg_cache_home, 'ansible-maint')
    else:
        working_dir = '~/.cache/ansible-maint'
    return os.path.expanduser(working_dir)


def get_github_token():
    token = os.getenv('GITHUB_TOKEN')
    if not token:
        token = getpass.getpass('GitHub Token: ')
    return token


def main():
    parser = create_parser()
    args = parser.parse_args()

    levels = {
        0: logging.WARNING,
        1: logging.INFO,
        2: logging.DEBUG,
    }
    logging.basicConfig(level=levels.get(args.verbose, logging.DEBUG))

    try:
        run_command('git --version')
    except OSError:
        LOGGER.error("Unable to execute required 'git' command")
        sys.exit(1)

    random.seed()

    if args.working_dir:
        working_dir = args.working_dir
    else:
        working_dir = get_default_working_dir()

    if args.timeout:
        token = get_github_token()
        if not token:
            LOGGER.warning("No token provided, maintainers won't be filtered by activity")
    else:
        token = None

    author_indexer = create_indexer(working_dir, reference=args.reference, namespace=args.namespace)

    maintainers = author_indexer.get_maintainers_for_namespace(args.namespace)

    if not maintainers:
        LOGGER.error("Maintainers not found, check 'namespace' parameter")
        sys.exit(1)

    LOGGER.info("\nMaintainers: %s (%s)", ' '.join(sorted(maintainers)), len(maintainers))

    if args.timeout and token:
        auth_headers = {
            'Authorization': 'token {}'.format(token),
        }
        active_maintainers = select_active_users(auth_headers, maintainers, days=args.timeout)
        print("\nActive maintainers: %s" % len(active_maintainers))
        print("\t%s" % ' '.join(sorted(active_maintainers)))
        selected = pick_contributors(active_maintainers)
    else:
        selected = pick_contributors(maintainers)
        print("\nSelected: %s" % ' '.join(selected))

    if selected:
        comment = "\n{selected} (as randomly selected maintainer of " \
                  "`{namespace}` namespace): could you please review this pull " \
                  "request ?"
        selected = ['@%s' % x for x in selected]
        comment = comment.format(selected=' '.join(selected), namespace=args.namespace)
        print(comment)


if __name__ == '__main__':
    main()
