// https://developers.google.com/web/updates/2015/04/cut-and-copy-commands
var copyBtn = document.querySelector('#copy');
copyBtn.hidden = !document.queryCommandSupported('copy');

copyBtn.addEventListener('click', function(event) {
  // Select the text
  var template = document.querySelector('.template');
  var range = document.createRange();
  range.selectNode(template);
  window.getSelection().addRange(range);

  try {
    // Now that we've selected the anchor text, execute the copy command
    var successful = document.execCommand('copy');
    var msg = successful ? 'successful' : 'unsuccessful';
    console.debug('Copy template command was ' + msg);
  } catch(err) {
    console.error('Unable to copy');
  }

  window.getSelection().removeRange(range);
});
