function filter_maintainers(button, days) {
    var buttons, namespace_maintainers, selected, end_period, activity, i, parts;
    
    if (button) {
        buttons = document.getElementsByClassName('pure-button-active');
        if (buttons.length) {
            buttons[0].classList.remove('pure-button-active');
        }
        button.classList.add('pure-button-active');
    }

    namespace_maintainers = document.getElementsByClassName('maintainers');
    namespace_maintainers = namespace_maintainers[0].getElementsByTagName('div');

    end_period = new Date();
    end_period.setDate(end_period.getDate() - days)

    selected = [];

    // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < namespace_maintainers.length; i++) {
        element = namespace_maintainers[i].getElementsByTagName('a')[0]
        activity = element.getAttribute('data-activity')

        if (days == 0) {
            namespace_maintainers[i].hidden = false;
            continue;
        }

        if (activity == "") {
            namespace_maintainers[i].hidden = true;
        } else {
            parts = activity.split('-');
            date = new Date(parts[0], parts[1]-1, parts[2]);
            namespace_maintainers[i].hidden = date.getTime() < end_period.getTime()
            if (! namespace_maintainers[i].hidden) {
                selected.push(element.getAttribute('data-login'));
            }
        }
    }

    // update random selection
    var index1, index2, random_maintainers;
    random_maintainers = document.getElementsByClassName('template')[0].getElementsByTagName('span');

    if (! random_maintainers.length) {
        return
    }

    if (days != 0) {
        if (selected.length) {
            index1 = Math.floor(Math.random() * selected.length);
            random_maintainers[0].textContent = selected[index1];

            if (selected.length > 1) {
                index2 = Math.floor(Math.random() * selected.length);
                random_maintainers[1].textContent = selected[index2];
            }
        }
    } else {
        if (namespace_maintainers.length) {
            index1 = Math.floor(Math.random() * namespace_maintainers.length);
            random_maintainers[0].textContent = namespace_maintainers[index1].getElementsByTagName('a')[0].getAttribute('data-login');

            if (namespace_maintainers.length > 1) {
                index2 = Math.floor(Math.random() * namespace_maintainers.length);
                random_maintainers[1].textContent = namespace_maintainers[index2].getElementsByTagName('a')[0].getAttribute('data-login');
            }
        }
    }
}
