// https://www.w3schools.com/howto/howto_js_filter_lists.asp
function filter_namespaces() {
    var input, filter, a, i;
    input = document.getElementById('search');
    filter = input.value.toUpperCase();
    namespaces = document.getElementById('namespaces-list');
    row = namespaces.getElementsByTagName('div');

    // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < row.length; i++) {
        a = row[i].getElementsByTagName("a")[0];
        if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
            row[i].hidden = false;
        } else {
            row[i].hidden = true;
        }
    }
}
