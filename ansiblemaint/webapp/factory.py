#!/usr/bin/env python
# encoding: utf-8

# Copyright: 2017 Pierre-Louis Bonicoli <pierre-louis@libregerbil.fr>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import absolute_import, division, print_function

import logging

from ansiblemaint.webapp.blueprints import maintainers

from flask import Flask, current_app


def create_app():
    app = Flask('ansiblemaint', static_folder='webapp/static',
                template_folder='webapp/templates')

    app.config.from_envvar('ANSIBLEMAINT_SETTINGS', silent=True)

    log_level = getattr(logging, app.config.get('LOG_LEVEL', 'WARNING'), 'WARNING')
    app.logger.setLevel(log_level)
    logging.basicConfig(level=log_level)

    app.jinja_env.trim_blocks = True
    app.jinja_env.lstrip_blocks = True

    app.register_blueprint(maintainers.bp, url_prefix='/maintainers')
    register_cli(app)

    return app


def register_cli(app):
    @app.cli.command('init_memcache')
    def init_memcache_command():
        """Populate memcache"""
        maintainers.init_memcache(current_app)
