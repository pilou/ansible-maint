#!/usr/bin/env python
# encoding: utf-8

# Copyright: 2017 Pierre-Louis Bonicoli <pierre-louis@libregerbil.fr>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from datetime import datetime
from functools import partial
from itertools import islice
import json
import random
import os
import shutil
import tempfile

from ansiblemaint import ansiblemaint
from ansiblemaint.version import VERSION

from flask import Blueprint, current_app, g, render_template
from pymemcache.client.base import Client
import pytz
import requests
import trollius


bp = Blueprint('maintainers', __name__)


def json_serializer(key, value):
    return json.dumps(value), 1


def json_deserializer(key, value, flags):
    if flags == 1:
        return json.loads(value)
    raise Exception("Unknown serialization format")


def get_memcache():
    """Opens a new memcache connection if there is none yet for the
    current application context.
    """
    if not hasattr(g, 'memcache'):
        g.memcache = Client(('localhost', 11211), serializer=json_serializer,
                            deserializer=json_deserializer)
    return g.memcache


@trollius.coroutine
def fetch_data(fetch, users):
    loop = trollius.get_event_loop()
    results = []
    futures = [
        loop.run_in_executor(None, fetch, maintainer)
        for maintainer in users
    ]

    for future in futures:
        result = yield trollius.From(future)
        results.append(result)
    raise trollius.Return(results)


def fetch_name(headers, login=None):
    logger = ansiblemaint.LOGGER
    logger.info('Fetch name for %r', login)

    url = '{}/users/{}'.format(ansiblemaint.API_URL, login)

    # first page contains the more recent events
    try:
        response = requests.get(url, headers=headers)
    except Exception:
        logger.error('Error while fetching name (%s)', login, exc_info=True)
        return

    if response.status_code == 404:
        logger.error('Invalid user %r', login)
        return
    elif response.status_code != 200:
        logger.error('HTTP status code: %r', response.status_code)
        if 'application/json' in response.headers.get('Content-Type', ''):
            logger.error('HTTP Response: %r', response.json())
        else:
            logger.error('HTTP Response: %r', response.text)
        logger.error('HTTP headers: %r', response.headers)
        remaining = response.headers['X-RateLimit-Remaining']

        # Check if rate limit has been reached
        if response.status_code == 403 and int(remaining) == 0:
            reset_time = datetime.fromtimestamp(int(response.headers['X-RateLimit-Reset']))
            logger.warn('Number of requests remaining %s (%s local time). '
                        'Rate limit will be reset at %s', remaining, datetime.now(), reset_time)
            return

    data = response.json()
    return {'login': login, 'name': data['name']}


def init_memcache(app):

    work_dir = os.getenv('WORKDIR') or app.config.get('WORKDIR')
    if not work_dir:
        temp_work_dir = True
        work_dir = tempfile.mkdtemp()
    else:
        temp_work_dir = False

    token = os.getenv('GITHUB_TOKEN') or app.config.get('GITHUB_TOKEN')

    allmaintainers = set()

    headers = {
        'Authorization': 'token {}'.format(token),
    }
    now = datetime.now(tz=pytz.utc).strftime("%Y-%m-%d %H:%M %Z")
    try:
        # all namespaces
        author_indexer = ansiblemaint.create_indexer(work_dir, namespace='')
        memcache = get_memcache()
        namespaces = list(author_indexer.list_namespaces())
        namespaces.sort()
        memcache.set('_namespaces', namespaces)
        for namespace in namespaces:
            maintainers = author_indexer.get_maintainers_for_namespace(namespace)
            memcache.set(namespace, maintainers)
            allmaintainers.update(maintainers)

        if len(allmaintainers) > 5000:  # The maximum number of requests you're permitted to make per hour.
            app.logger.warn('GitHub rate limit might be reached')

        loop = trollius.get_event_loop()
        start = 0
        while True:
            elements = set(maintainer for maintainer in islice(allmaintainers, start, start+20))

            if elements:
                start += len(elements)

                _fetch_activity = partial(ansiblemaint.fetch_activity, headers, 45)
                results = loop.run_until_complete(fetch_data(_fetch_activity, elements))
                for activity in results:
                    if activity:
                        user = activity.pop('login', None)
                        if user and activity:
                            memcache.set('_%s_activity' % user, activity)

                _fetch_name = partial(fetch_name, headers)
                results = loop.run_until_complete(fetch_data(_fetch_name, elements))
                for result in results:
                    if result:
                        memcache.set('_%s_name' % result['login'], result['name'])
            else:
                break

        memcache.set('_last_updated', now)
    finally:
        if temp_work_dir:
            shutil.rmtree(work_dir)


def get_comment(namespace):
    elements = [
        ('Hi!', 'Hello!', 'Good day!'),
        (('(as randomly selected maintainer of `{}`namespace)'.format(namespace),) + ('',) * 9),
        ('could you please review', 'could you take a look at'),
        ('this contribution?', 'this pull request?'),
    ]
    return random.choice(elements[0]), ' '.join((random.choice(parts) for parts in elements[1:]))


def footer(memcache):
    return {
        'last_updated': memcache.get('_last_updated'),
        'version': VERSION,
        'contact': current_app.config.get('CONTACT')
    }


@bp.route('/')
def select():
    memcache = get_memcache()
    namespaces = memcache.get('_namespaces') or []
    return render_template('select.html', namespaces=namespaces, **footer(memcache))


@bp.route('/<path:namespace>')
def list_ns(namespace=None):
    if not namespace:
        return

    memcache = get_memcache()
    maintainers = memcache.get(namespace) or []
    maint_last_activity = {}
    for maintainer in maintainers:
        if maintainer == 'ansible':
            continue
        last_activity = memcache.get('_%s_activity' % maintainer)
        if last_activity:
            last_activity = datetime.strptime(last_activity['created_at'], '%Y-%m-%dT%H:%M:%SZ').date()
        else:
            last_activity = ''
        name = memcache.get('_%s_name' % maintainer)
        maint_last_activity[maintainer] = {'name': name, 'activity': last_activity}
    selections = ansiblemaint.pick_contributors(maint_last_activity)

    prefix, suffix = get_comment(namespace)
    return render_template('maintainers.html', namespace=namespace, maintainers=maint_last_activity,
                           selections=selections, prefix=prefix, suffix=suffix, **footer(memcache))
